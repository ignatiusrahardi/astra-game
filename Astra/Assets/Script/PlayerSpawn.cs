﻿using UnityEngine;
using System.Collections;

public class PlayerSpawn : MonoBehaviour
{
    private int spawnPoint;
    public Vector3[] spawnPos = new Vector3[9];
    public Vector3[] cameraPos = new Vector3[9];
    public GameObject player;
    public GameObject slimo;
    public Camera mainCamera;

    void Start()
    {
        SpawnPlayer();
    }

    private void SpawnPlayer()
    {
        spawnPoint = Random.Range(0, spawnPos.Length);
        player.transform.position = spawnPos[spawnPoint];
        slimo.GetComponent<SlimoSpawn>().SpawnSlimo(spawnPoint);
        mainCamera.transform.position = cameraPos[spawnPoint];
    }

    public int GetSpawnPoint()
    {
        return spawnPoint;
    }
}

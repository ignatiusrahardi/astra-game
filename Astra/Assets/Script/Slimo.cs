﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Pathfinding;

public class Slimo : MonoBehaviour
{
    public GameObject poopPrefab1;
    public GameObject poopPrefab2;
    public GameObject poopPrefab3;
    public float spawnTime;
    public float destroyTime;
    public float startFreezeTime;
    public GameObject canvasTime;
    private float freezeTime;
    private bool isFreezing;
    private TimerGame timer;
    Animator anim;
    AIPath aiPath;
    GameObject[] poops = new GameObject[3];
    System.Random random = new System.Random();

    // Start is called before the first frame update
    void Start()
    {
        aiPath = GetComponent<AIPath>();
        anim = GetComponentInChildren<Animator>();
        timer = canvasTime.GetComponent<TimerGame>();
        poops[0] = poopPrefab1;
        poops[1] = poopPrefab2;
        poops[2] = poopPrefab3;
        SpawnTrail();
        StartCoroutine(RenderTrails());
        isFreezing = false;
        freezeTime = startFreezeTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFreezing)
        {
            anim.SetBool("isFreezing", true);
            aiPath.canMove = false;
            freezeTime -= Time.deltaTime;
            if (freezeTime <= 0f)
            {
                freezeTime = startFreezeTime;
                isFreezing = false;
                anim.SetBool("isFreezing", false);
            }
        } else
        {
            aiPath.canMove = true;
        }
        aiPath.maxSpeed += timer.GetTime() * 0.000001f;
    }

    void SpawnTrail()
    {
        GameObject poop = Instantiate(poops[random.Next(0,3)]) as GameObject;
        poop.transform.position = aiPath.position;

        StartCoroutine(DestroyTrails(poop));
    }

    IEnumerator RenderTrails()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnTime);
            SpawnTrail();
        }
    }

    IEnumerator DestroyTrails(GameObject poop)
    {
        yield return new WaitForSeconds(destroyTime);
        Destroy(poop);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ice")
        {
            isFreezing = true;
        }
    }
}

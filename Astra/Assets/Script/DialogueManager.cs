﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueManager : MonoBehaviour
{

    public TextAsset textFile;
    public string[] textLines;

    public GameObject dialogueBox;
    public Text dialogue;
    public Text space;

    private int current;
    private int end;

    private bool isTyping = false;
    private bool cancelTyping = false;

    public float typingSpeed;
    public Animator trans;

    public BackgroundManager manager;
    public AudioSource play;
    public AudioSource blip;
    public AudioSource next;

    // Start is called before the first frame update
    void Start()
    {
        if (textFile != null)
        {
            textLines = (textFile.text.Split('\n'));
            end = textLines.Length - 1;
        }

        current = 0;
        space.gameObject.SetActive(false);
        StartCoroutine(TextScroll(textLines[current]));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!isTyping)
            {
                current++;
                manager.ChangeBackground(current);
                if (current > end)
                {
                    space.gameObject.SetActive(true);
                    
                    StartCoroutine(Trans());
                }
                else
                {
                    space.gameObject.SetActive(false);
                    //next.Play();
                    StartCoroutine(TextScroll(textLines[current]));
                   

                }
            }
            else if (isTyping && !cancelTyping)
            {
                cancelTyping = true;
                space.gameObject.SetActive(true);
            }
        }
    }

    private IEnumerator TextScroll (string text)
    {

        int letter = 0;
        dialogue.text = "";
        isTyping = true;
        cancelTyping = false;
        while (isTyping && !cancelTyping && (letter < text.Length - 1))
        {
            dialogue.text += text[letter];
            letter++;
            blip.Play();
            yield return new WaitForSeconds(typingSpeed);
            
        }
        dialogue.text = text;
        isTyping = false;
        cancelTyping = false;
        space.gameObject.SetActive(true);
    }
    private IEnumerator Trans()
    {
        trans.SetTrigger("end");
        play.Play();
        yield return new WaitForSeconds(3f);
        dialogueBox.SetActive(false);
        SceneManager.LoadScene(7);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Animator transation;
    void Start()
    {
        ///Time.timeScale = 1f;
    }
    public void PlayGame()
    {
        StartCoroutine(StartGame());
    }

    IEnumerator StartGame()
    {
        transation.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(5);
    }

    public void Retry()
    {
        StartCoroutine(RetryGame());
    }

    IEnumerator RetryGame()
    {
        transation.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(1);
    }

    // quit func
    public void QuitGame()
    {
        Debug.Log("quit game");
        Application.Quit();
    }

    public void toMenu()
    {
        StartCoroutine(Menu());

    }

    IEnumerator Menu()
    {
        transation.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(0);
    }
}

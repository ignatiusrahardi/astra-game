﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shooting : MonoBehaviour
{
    public GameObject laserPrefab;
    public GameObject freezePrefab;
    public GameObject player;
    public GameObject pickupEffect;
    public SpriteRenderer spriteR;
    public AudioSource audio;
    public AudioClip atk;
    public AudioClip powerUp;

    public float laserAmmo = 0;
    public float freezeAmmo = 0;
    public float bulletForce = 20f;
    public float fireRate = 0.5F;
    //private float nextFire = 0.0F;
    public bool wall = false;
    public bool laserPw = true;
    public bool freezePw = false;
    public Text text;


    // Update is called once per frame
    
    void Update()
    {

        string wording = "";
    
        

        if (freezeAmmo == 0)
        {
            freezePw = false;
            if (laserAmmo != 0) {
                laserPw = true;
            }
        }

        if (Input.GetButtonDown("Fire2") && (laserAmmo != 0 && freezeAmmo != 0))
        {
            laserPw = !laserPw;
            freezePw = !freezePw;
        }

        if (laserPw)
        {
            wording += "Power Up : HeatUp\n";
            wording += "Ammo : " + laserAmmo + "\n";
            
            text.text = wording;
        }
        else if (freezePw)
        {
            wording += "Power Up : FreezUp\n";
            wording += "Ammo : " + freezeAmmo + "\n";

            text.text = wording;
        }
        else if (laserPw == false && freezePw == false)
        {
            
            wording += "\n";
            text.text = wording;
        }

        if (laserAmmo == 0)
        {
            laserPw = false;
            if (freezeAmmo != 0)
            {
                freezePw = true;
            }
            
        }
        if (freezeAmmo == 0)
        {
            freezePw = false;
            if (laserAmmo != 0)
            {
                laserPw = true;
            }
           
        }
        //if (Input.GetButtonDown("Fire2"))
        //{
        //    Debug.Log("masuk");
        //    if (laserPw == true)
        //    {
        //        freezePw = true;
        //        laserPw = false;
        //    } 
        //    else if (freezePw == true)
        //    {
        //        laserPw = true;
        //        freezePw = false;

        //    }
        //}

        if (Input.GetButtonDown("Fire1") && wall == false)
        {
            if (freezePw)
            {
                audio.PlayOneShot(atk);
                
                ShootFreeze();
            }
            else if (laserPw)
            {
                audio.PlayOneShot(atk);
                ShootLaser();
            }
        }

        
    }
    void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.collider.gameObject.name == "Tilemap_Collide" || collision.collider.gameObject.name == "Tilemap_Decoration")
        {
            wall = true;
        }
        if (collision.collider.gameObject.tag == "Ice")
        {
            if (freezeAmmo < 5)
            {
                
                freezePw = true;
                freezeAmmo = 5;
                laserPw = false;
                GameObject effect = Instantiate(pickupEffect, transform.position, Quaternion.identity);
                audio.PlayOneShot(powerUp);
                Destroy(effect, 0.5f);
                Destroy(collision.collider.gameObject);
            }

        }
        if (collision.collider.gameObject.tag == "Laser")
        {
            if (laserAmmo < 10)
            {
                
                laserPw = true;
                laserAmmo = 10;
                freezePw = false;
                GameObject effect = Instantiate(pickupEffect, transform.position, Quaternion.identity);
                audio.PlayOneShot(powerUp);
                Destroy(effect, 0.5f);
                Destroy(collision.collider.gameObject);
            }

        }
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log(collision.collider.gameObject.name);
        if (collision.collider.gameObject.name == "Tilemap_Collide" || collision.collider.gameObject.name == "Tilemap_Decoration")
        {
            wall = false;
        }
    }
        void ShootLaser()
    {
        
        GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity);
        Rigidbody2D laserRb = laser.GetComponent<Rigidbody2D>();
        Vector2 movement = player.GetComponent<PlayerMovement>().movement;
        float vektorHorizontal = player.GetComponent<PlayerMovement>().movement.x;
        float vektorVertikal = player.GetComponent<PlayerMovement>().movement.y;
        if (movement != Vector2.zero)
        {
            if (vektorHorizontal == 1.0)
            {
                laserRb.AddForce(Vector3.right * bulletForce, ForceMode2D.Impulse);
                laser.transform.Translate(Vector3.right * 50 * Time.deltaTime);
                laser.transform.Rotate(0f, 0f, 0f);
                laserAmmo -= 1;

                
            }
            else if (vektorHorizontal == -1.0)
            {
                laserRb.AddForce(Vector3.left * bulletForce, ForceMode2D.Impulse);
                laserRb.transform.Translate(Vector3.left * 50 * Time.deltaTime);
                laserRb.transform.Rotate(0f, 0f, 180f);
                laserAmmo -= 1;
            }
            else if (vektorVertikal == -1.0)
            {
                laserRb.AddForce(Vector3.down * bulletForce, ForceMode2D.Impulse);
                laserRb.transform.Translate(Vector3.down * 50 * Time.deltaTime);
                laserRb.transform.Rotate(0f, 0f, -90f);
                laserAmmo -= 1;
            }
            else if (vektorVertikal == 1.0)
            {
                laserRb.AddForce(Vector3.up * bulletForce, ForceMode2D.Impulse);
                laserRb.transform.Translate(Vector3.up * 50 * Time.deltaTime);
                laserRb.transform.Rotate(0f, 0f, 90f);
                laserAmmo -= 1;
            }
            else
            {
                Destroy(laser);
            }
        }
        else
        {
            // sprite renderer
            if (spriteR.sprite.name == "player_87")
            {
                laserRb.AddForce(Vector3.right * bulletForce, ForceMode2D.Impulse);
                laser.transform.Translate(Vector3.right * 50 * Time.deltaTime);
                laser.transform.Rotate(0f, 0f, 0f);
                laserAmmo -= 1;

            }
            else if (spriteR.sprite.name == "player_69")
            {
                laserRb.AddForce(Vector3.left * bulletForce, ForceMode2D.Impulse);
                laserRb.transform.Translate(Vector3.left * 50 * Time.deltaTime);
                laserRb.transform.Rotate(0f, 0f, 180f);
                laserAmmo -= 1;

            }
            else if (spriteR.sprite.name == "player_78")
            {
                laserRb.AddForce(Vector3.down * bulletForce, ForceMode2D.Impulse);
                laserRb.transform.Translate(Vector3.down * 50 * Time.deltaTime);
                laserRb.transform.Rotate(0f, 0f, -90f);
                laserAmmo -= 1;

            }
            else if (spriteR.sprite.name == "player_60")
            {
                laserRb.AddForce(Vector3.up * bulletForce, ForceMode2D.Impulse);
                laserRb.transform.Translate(Vector3.up * 50 * Time.deltaTime);
                laserRb.transform.Rotate(0f, 0f, 90f);
                laserAmmo -= 1;

            }
            else
            {
                Debug.Log(spriteR.sprite.name);

                Destroy(laser);
            }
        }
        
        //laserRb.velocity = firePoint.up * bulletForce
        
    }
    void ShootFreeze()
    {

        GameObject freeze = Instantiate(freezePrefab, transform.position, Quaternion.identity);
        Rigidbody2D freezeRb = freeze.GetComponent<Rigidbody2D>();
        Vector2 movement = player.GetComponent<PlayerMovement>().movement;
        float vektorHorizontal = player.GetComponent<PlayerMovement>().movement.x;
        float vektorVertikal = player.GetComponent<PlayerMovement>().movement.y;
        if (movement != Vector2.zero)
        {
            if (vektorHorizontal == 1.0)
            {
                freezeRb.AddForce(Vector3.right * bulletForce, ForceMode2D.Impulse);
                freezeRb.transform.Translate(Vector3.right * 50 * Time.deltaTime);
                freezeRb.transform.Rotate(0f, 0f, 0f);
                freezeAmmo -= 1;

            }
            else if (vektorHorizontal == -1.0)
            {
                freezeRb.AddForce(Vector3.left * bulletForce, ForceMode2D.Impulse);
                freezeRb.transform.Translate(Vector3.left * 50 * Time.deltaTime);
                freezeRb.transform.Rotate(0f, 0f, 180f);
                freezeAmmo -= 1;
            }
            else if (vektorVertikal == -1.0)
            {
                freezeRb.AddForce(Vector3.down * bulletForce, ForceMode2D.Impulse);
                freezeRb.transform.Translate(Vector3.down * 50 * Time.deltaTime);
                freezeRb.transform.Rotate(0f, 0f, -90f);
                freezeAmmo -= 1;
            }
            else if (vektorVertikal == 1.0)
            {
                freezeRb.AddForce(Vector3.up * bulletForce, ForceMode2D.Impulse);
                freezeRb.transform.Translate(Vector3.up * 50 * Time.deltaTime);
                freezeRb.transform.Rotate(0f, 0f, 90f);
                freezeAmmo -= 1;
            }
            else
            {
                Destroy(freeze);
            }
        }
        else
        {
            // sprite renderer
            if (spriteR.sprite.name == "player_87")
            {
                freezeRb.AddForce(Vector3.right * bulletForce, ForceMode2D.Impulse);
                freezeRb.transform.Translate(Vector3.right * 50 * Time.deltaTime);
                freezeRb.transform.Rotate(0f, 0f, 0f);
                freezeAmmo -= 1;

            }
            else if (spriteR.sprite.name == "player_69")
            {
                freezeRb.AddForce(Vector3.left * bulletForce, ForceMode2D.Impulse);
                freezeRb.transform.Translate(Vector3.left * 50 * Time.deltaTime);
                freezeRb.transform.Rotate(0f, 0f, 180f);
                freezeAmmo -= 1;

            }
            else if (spriteR.sprite.name == "player_78")
            {
                freezeRb.AddForce(Vector3.down * bulletForce, ForceMode2D.Impulse);
                freezeRb.transform.Translate(Vector3.down * 50 * Time.deltaTime);
                freezeRb.transform.Rotate(0f, 0f, -90f);
                freezeAmmo -= 1;

            }
            else if (spriteR.sprite.name == "player_60")
            {
                freezeRb.AddForce(Vector3.up * bulletForce, ForceMode2D.Impulse);
                freezeRb.transform.Translate(Vector3.up * 50 * Time.deltaTime);
                freezeRb.transform.Rotate(0f, 0f, 90f);
                freezeAmmo -= 1;

            }
            else
            {
                
                Destroy(freeze);
            }
        }

        //laserRb.velocity = firePoint.up * bulletForce

    }
}

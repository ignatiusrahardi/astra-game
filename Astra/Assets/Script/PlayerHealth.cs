﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public GameObject player;
    public float startingHealth = 10f;
    public Animator anim;
    public float maxHealth = 10f;
    public float currentHealth = 10f;
    public int healthReg;
    public AudioSource audio;
    public AudioClip ouch;
    public AudioClip dead;
    private Material matWhite;
    private Material matDef;
    bool isRegenHealth;
    PlayerMovement movement;

    // Update is called once per frame
    private void Start()
    {
        movement = player.GetComponent<PlayerMovement>();
    }
    void Update()
    {
        if (currentHealth <= 0)
        {
            movement.move = false;
            movement.movement = Vector2.zero;
            TimerGame.timer = 0.0f;
            StartCoroutine(GameOver());
        }
        if (currentHealth != startingHealth && !isRegenHealth)
        {
            StartCoroutine(RegainHealthOverTime());
        }
    }

    void OnCollisionEnter2D(Collision2D other) {
        Collider2D collider = other.collider ;
        if(collider.tag == "Enemy") {
            TakeDamage(collider.GetComponent<Damage>().damage);
        }
    }


    public void TakeDamage(float damageValue)
    {
        
        currentHealth -= damageValue;
        StartCoroutine(Dmg());


    }

    private IEnumerator GameOver()
    {
        audio.PlayOneShot(dead);
        anim.SetTrigger("dead");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(4);
    }

    private IEnumerator Dmg()
    {
        audio.PlayOneShot(ouch);
        anim.SetTrigger("damage");
        yield return new WaitForSeconds(1f);
    }

    private IEnumerator RegainHealthOverTime()
    {
        isRegenHealth = true;
        while (currentHealth < startingHealth)
        {
            Healthregen();
            yield return new WaitForSeconds(10);
        }
        isRegenHealth = false;
    }


    public void Healthregen()
    {
        currentHealth += healthReg;

    }

    public void Heal(float restore)
    {
        currentHealth += restore;
        if (currentHealth >= 100)
        {
            currentHealth = 100;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSFX : MonoBehaviour
{
    public AudioSource button;
    public AudioClip hover;
    public AudioClip click;
    // Start is called before the first frame update
   public void Hover()
    {
        button.PlayOneShot(hover);
    }
    public void Click()
    {
        button.PlayOneShot(click);
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainDoor : MonoBehaviour
{
    public Sprite spriteOpen;
    public Animator transation;
    public AudioSource door;
    public AudioSource keycard;
    public AudioSource denied;
    private bool isOpen;
    public int scene;

    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.collider.name);
        if(isOpen)
        {
            if (collision.collider.name == "Player")
            {
                StartCoroutine(change());
            }
        }
    }

    IEnumerator change()
    {
        transation.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(scene);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player") {
            if (collision.GetComponent<PlayerMovement>().keycard == 3)
            {
                keycard.Play();
                door.Play();
                GetComponent<SpriteRenderer>().sprite = spriteOpen;
                isOpen = true;
            }
            else
            {
                denied.Play();
            }
        }
    }
}

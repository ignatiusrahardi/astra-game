﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class SlimoSprite : MonoBehaviour
{
    public AIPath aiPath;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("moveX", aiPath.desiredVelocity.x);
        anim.SetFloat("moveY", aiPath.desiredVelocity.y);
    }
}

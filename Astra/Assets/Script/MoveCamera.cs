﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{

    Transform point;

    // Start is called before the first frame update
    void Start()
    {
        point = transform.parent.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player")
        { 
            GameObject cam = GameObject.Find("Main Camera");
            cam.transform.position = point.position;
        }
    }
}

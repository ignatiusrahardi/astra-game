﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float speed;
    private Rigidbody2D rb;
    private bool moving;

    public float timeBetweenMove;
    private float timeBetweenMoveCounter;
    public float timeToMove;
    private float timeToMoveCounter;

    private Vector3 direction;

    public Transform player;
    public float maxRange = 5f;
    public float minRange = 0.75f;
    public bool isAttacking;
    private Animator anim;

    public AudioSource sound;
    public AudioSource hitSound;
    private bool soundPlaying;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);
        timeToMoveCounter = Random.Range(timeToMove * 0.75f, timeToMove * 1.25f);
        anim = GetComponent<Animator>();
        isAttacking = false;
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("isMoving", moving);
        if (Vector3.Distance(player.position, transform.position) <= maxRange && Vector3.Distance(player.position, transform.position) >= minRange)
        {
            moving = true;
            AdjustAttackAnimation();
            if (!soundPlaying)
            {
                soundPlaying = true;
                StartCoroutine(PlaySound());
            }
            FollowPlayer();
        } else
        {
            if (moving)
            {
                timeToMoveCounter -= Time.deltaTime;
                rb.velocity = direction;
                AdjustWalkAnimation();

                if (timeToMoveCounter < 0f)
                {
                    moving = false;
                    timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);
                }
            }
            else
            {
                timeBetweenMoveCounter -= Time.deltaTime;
                rb.velocity = Vector2.zero;

                if (timeBetweenMoveCounter < 0f)
                {
                    moving = true;
                    timeToMoveCounter = Random.Range(timeToMove * 0.75f, timeToMove * 1.25f);
                    direction = new Vector3(Random.Range(-1f, 1f) * speed, Random.Range(-1f, 1f) * speed, 0f).normalized;
                }
            }
        }

        if (isAttacking)
        {
            hitSound.Play();
            anim.SetBool("isAttacking", true);
        } else
        {
            anim.SetBool("isAttacking", false);
        }
    }

    public void FollowPlayer()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
    }
    
    public void AdjustWalkAnimation()
    {
        anim.SetFloat("MoveX", direction.x);
        anim.SetFloat("MoveY", direction.y);
    }

    public void AdjustAttackAnimation()
    {
        anim.SetFloat("MoveX", player.transform.position.x - transform.position.x);
        anim.SetFloat("MoveY", player.transform.position.y - transform.position.y);
    }

    IEnumerator PlaySound()
    {
        sound.Play();
        yield return new WaitForSeconds(5f);
        soundPlaying = false;
        sound.Stop();
    }
}

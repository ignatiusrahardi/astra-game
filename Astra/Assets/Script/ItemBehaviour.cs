﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ItemBehaviour : MonoBehaviour
{
    private bool ignore = false;
    public BoxCollider2D collid;
    // Start is called before the first frame update
    

    // Update is called once per frame
    void Update()
    {
        
        if (ignore)
        {
            Debug.Log(ignore);
            Debug.Log("masuk");
            StartCoroutine(Collid());
            ignore = false;
        }
        
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.tag == "Player")
        {
            if (collision.collider.GetComponent<Shooting>().freezeAmmo == 5 && this.gameObject.tag == "Ice")
            {
                
                collid.enabled = false;
                ignore = true;
            }
            else if(collision.collider.GetComponent<Shooting>().laserAmmo == 10 && this.gameObject.tag == "Laser")
            {
                collid.enabled = false;
                ignore = true;
            }

        }
       
    }
    IEnumerator Collid()
    {
        

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(2);
        collid.enabled = true;
        
        
    }
}

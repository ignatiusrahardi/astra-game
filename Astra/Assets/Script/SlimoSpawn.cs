﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimoSpawn : MonoBehaviour
{
    public Vector3[] spawnPos = new Vector3[9];
    public GameObject slimo;

    public void SpawnSlimo(int point)
    {
        slimo.transform.position = spawnPos[point];
    }
}

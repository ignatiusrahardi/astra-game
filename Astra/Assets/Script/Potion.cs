﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : MonoBehaviour
{

    public float restore = 1f;
    public AudioClip pickupSound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player")
        {
            collision.GetComponent<PlayerHealth>().Heal(restore);
            collision.GetComponent<AudioSource>().PlayOneShot(pickupSound);
            Destroy(this.gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    public float damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player" && (gameObject.name.Contains("Bullet") || gameObject.name.Contains("Slimo")))
        {
            collision.GetComponent<PlayerHealth>().TakeDamage(damage);
        }
    }

}

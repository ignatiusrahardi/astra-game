﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Shoot : MonoBehaviour
{
    public GameObject hitIce;
    public GameObject hitLaser;
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.name != "Player")
        {
            Debug.Log(collision.collider.gameObject.name);
            if (this.gameObject.tag == "Ice")
            {
                Debug.Log("masuk");
                Destroy(this.gameObject);
                GameObject effect = Instantiate(hitIce, transform.position, Quaternion.identity);
                Destroy(effect, 0.5f);
                
            }
            if (this.gameObject.tag == "Laser")
            {
                Debug.Log("masuk");
                Destroy(this.gameObject);
                GameObject effect = Instantiate(hitLaser, transform.position, Quaternion.identity);
                Destroy(effect, 0.5f);
                
            }

        }
        
    }
    
}

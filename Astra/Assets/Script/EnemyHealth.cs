﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public float maxHealth = 2f;
    public float currentHealth = 2f;
    private Material matWhite;
    private Material matDef;
    SpriteRenderer sr;

    private void Start()
    {
        sr = this.gameObject.GetComponent<SpriteRenderer>();
        matWhite = Resources.Load("RedFlash", typeof(Material)) as Material;
        matDef = sr.material;
    }

    private void Update()
    {
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void TakeDamage(float damageValue)
    {
        if (currentHealth <= 0)
        {
            currentHealth = 0;
        }
        else
        {
            
            StartCoroutine(Dmg());
            currentHealth -= damageValue;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Laser")
        {
            TakeDamage(1);
        }
        else if (collision.collider.tag == "Ice")
        {
            GetComponent<EnemyMovementRange>().Freeze();
        }
    }
    IEnumerator Dmg()
    {
        sr.material = matWhite;
        yield return new WaitForSeconds(0.5f);
        sr.material = matDef;
    }
}

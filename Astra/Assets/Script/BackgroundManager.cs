﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundManager : MonoBehaviour
{
    private Image background;
    public Sprite[] sprites = new Sprite[8];
    int image = 0;

    void Start()
    {
        background = GetComponent<Image>();   
    }

    public void ChangeBackground(int line)
    {
        switch (line)
        {
            case 3:
                image = 1;
                break;
            case 4:
                image = 2;
                break;
            case 5:
                image = 3;
                break;
            case 6:
                image = 4;
                break;
            case 8:
                image = 5;
                break;
            case 10:
                image = 6;
                break;
            case 14:
                image = 7;
                break;
            default:
                break;
        }

        background.sprite = sprites[image];
    }
}

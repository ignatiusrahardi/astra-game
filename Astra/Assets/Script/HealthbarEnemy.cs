﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarEnemy : MonoBehaviour
{
    //[SerializeField]
    private EnemyHealth enemyHealth;
    [SerializeField]
    public Image barImage;
    [SerializeField]
    public Text valueText;
    [SerializeField]
    private Slider slider;


    void Awake()
    {
        slider = GetComponent<Slider>();
        enemyHealth = GetComponentInParent<EnemyHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        if (slider.value <= slider.minValue)
        {
            barImage.enabled = false;
        }

        if (slider.value > slider.minValue && !barImage.enabled)
        {
            barImage.enabled = true;
        }

        float barValue = enemyHealth.currentHealth / enemyHealth.maxHealth;

        if (barValue <= slider.maxValue / 2)
        {
            barImage.color = Color.red;
        }
        else
        {
            barImage.color = Color.green;
        }
        
        slider.value = barValue;
        valueText.text = (barValue * 100).ToString();
    }
}

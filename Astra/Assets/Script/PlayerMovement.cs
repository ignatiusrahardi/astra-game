﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    public float moveSpeed = 5f;

    public Rigidbody2D rb;
    public Animator anim;
    public int keycard = 0;
    public Text text;
    public GameObject player;
    public Vector2 movement;
    public Vector2 aim;
    public bool move = true;
    // Update is called once per frame
    void Update()
    {
        // input
        //print(move);
        if (move)
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");
            bool freeze = player.GetComponent<Shooting>().freezePw;
            bool laser = player.GetComponent<Shooting>().laserPw;
            bool wall =player.GetComponent<Shooting>().wall;
            movement = movement.normalized;
            text.text = "Keycard: " + keycard + "/3";

            if (movement != Vector2.zero && wall == false)
            {
                anim.SetFloat("Horizontal", movement.x);
                anim.SetFloat("Vertical", movement.y);
                if (freeze || laser)
                {
                    anim.SetBool("IsAttacking", Input.GetButtonDown("Fire1"));
                }

            }
            anim.SetFloat("Speed", movement.sqrMagnitude);

            if ((freeze || laser) && wall == false)
            {
                anim.SetBool("IsAttacking", Input.GetButtonDown("Fire1"));
            }
        }
    }

    void FixedUpdate()
    {
        //movement
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

    }

    public void TakeKeycard()
    {
        keycard++;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cont : MonoBehaviour
{
    public Animator trans;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(Trans());
        }
    }

    private IEnumerator Trans()
    {
        trans.SetTrigger("end");
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(1);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementRange : MonoBehaviour
{
    public float speed;
    private Rigidbody2D rb;
    private bool moving;

    public float timeBetweenMove;
    private float timeBetweenMoveCounter;
    public float timeToMove;
    private float timeToMoveCounter;

    private Animator anim;
    private Vector3 direction;

    public Transform player;
    public float maxRange = 5f;
    public float minRange = 0.75f;
    public bool isAttacking;
    public bool isFreezing;
    public GameObject bulletPrefab;
    public AudioSource sound;
    public AudioSource shootShound;

    private float timeBetweenAttacks;
    public float startTimeBetweenAttacks;

    private float freezeTime;
    public float startFreezeTime;
    private bool soundPlaying;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);
        timeToMoveCounter = Random.Range(timeToMove * 0.75f, timeToMove * 1.25f);
        anim = GetComponent<Animator>();
        isAttacking = false;
        isFreezing = false;
        timeBetweenAttacks = 0;
        freezeTime = startFreezeTime;
        soundPlaying = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAttacking && !isFreezing)
        {
            anim.SetBool("isAttacking", false);
            if (Vector3.Distance(player.position, transform.position) <= maxRange && Vector3.Distance(player.position, transform.position) >= minRange)
            {
                if (!soundPlaying)
                {
                    soundPlaying = true;
                    StartCoroutine(PlaySound());
                }
                FollowPlayer();
            }
            else
            {
                if (moving)
                {
                    timeToMoveCounter -= Time.deltaTime;
                    rb.velocity = direction;
                    AdjustAnimation();

                    if (timeToMoveCounter < 0f)
                    {
                        moving = false;
                        timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);
                    }
                }
                else
                {
                    timeBetweenMoveCounter -= Time.deltaTime;
                    rb.velocity = Vector2.zero;

                    if (timeBetweenMoveCounter < 0f)
                    {
                        moving = true;
                        timeToMoveCounter = Random.Range(timeToMove * 0.75f, timeToMove * 1.25f);
                        direction = new Vector3(Random.Range(-1f, 1f) * speed, Random.Range(-1f, 1f) * speed, 0f).normalized;
                    }
                }
            }
            timeBetweenAttacks -= Time.deltaTime;
        }
        else if (isAttacking && !isFreezing)
        {
            if (timeBetweenAttacks < 0f)
            {
                Instantiate(bulletPrefab, transform.position, Quaternion.identity);
                shootShound.Play();
                timeBetweenAttacks = startTimeBetweenAttacks;
            } else
            {
                timeBetweenAttacks -= Time.deltaTime;
            }
            anim.SetBool("isAttacking", true);
            AdjustAnimation();
        }
        else
        {
            rb.velocity = Vector3.zero;
            freezeTime -= Time.deltaTime;
            if (freezeTime <= 0f)
            {
                freezeTime = startFreezeTime;
                isFreezing = false;
            }
        }
    }

    public void FollowPlayer()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        AdjustAnimation();
    }

    public void AdjustAnimation()
    {
        if (player.transform.position.x - transform.position.x < 0f || direction.x < 0f)
        {
            anim.SetFloat("MoveX", 1);
        }
        else if (player.transform.position.x - transform.position.x > 0f || direction.x > 0f)
        {
            anim.SetFloat("MoveX", 0);
        }
    }

    public void SetTimeBetweenAttacks(float time)
    {
        timeBetweenAttacks = time;
    }

    public void Freeze()
    {
        isFreezing = true;
    }

    IEnumerator PlaySound()
    {
        sound.Play();
        yield return new WaitForSeconds(1f);
        soundPlaying = false;
        sound.Stop();
    }
}

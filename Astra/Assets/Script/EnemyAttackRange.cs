﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackRange : MonoBehaviour
{
    public EnemyMovementRange enemyMovementRange;
    public Transform player;
    public float force;

    private void Start()
    {
        enemyMovementRange = transform.GetComponentInParent<EnemyMovementRange>();
    }

    private void Update()
    {
        //Vector3 playerDir = player.position - transform.position;
        //float angle = Mathf.Atan2(playerDir.y, playerDir.x) * Mathf.Deg2Rad - 90f;
        //Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 90 * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            Debug.Log("Entering Trigger");
            enemyMovementRange.isAttacking = true;

           // bullet.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0f, force));
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            Debug.Log("Exiting Trigger");
            enemyMovementRange.isAttacking = false;
//            enemyMovementRange.SetTimeBetweenAttacks(0);
        }
    }
}

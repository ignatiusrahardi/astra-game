﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundManagerWin : MonoBehaviour
{
    private Image background;
    public Sprite[] sprites = new Sprite[3];
    int image = 0;

    void Start()
    {
        background = GetComponent<Image>();
    }

    public void ChangeBackground(int line)
    {
        switch (line)
        {
            case 2:
                image = 1;
                break;
            case 3:
                image = 2;
                break;
            default:
                break;
        }

        background.sprite = sprites[image];
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerGame : MonoBehaviour
{
    [SerializeField] private Text uiText;

    public static float timer;
    private bool canCount = true;
    private bool doOnce = false;

    // Start is called before the first frame update
    void Start()
    {
        if (this == null)
        {
            timer = 0.0f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (timer >= 0.0f && canCount)
        {
            timer += Time.deltaTime;
            double min = System.Math.Floor( timer / 60 );
            double sec = System.Math.Floor( (timer - min*60) * 100) / 100;
            uiText.text = string.Format("Time Elapsed : {0} : {1}", min, sec);
            System.Diagnostics.Debug.WriteLine(min.ToString(), sec.ToString());
        }

        else if (timer <= 0.0f && doOnce)
        {
            canCount = false;
            doOnce = true;
            uiText.text = "0:00.00";
            timer = 0.0f;
            // Change scene :);
        }
    }
    void Awake()
    {
        // Do not destroy this game object:
        DontDestroyOnLoad(this);
    }

    public float GetTime()
    {
        return timer;
    }
}